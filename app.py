from flask import Flask, render_template, request, json
import json
import pandas as pd
from collections import Counter
from flask_scss import Scss
import itertools

app = Flask(__name__)
Scss(app)


@app.route('/')
def home():
    
    global principal

    with open('MONTECASINO_ALL.json') as f:
        principal = json.load(f)
    #principal=json.dumps(principal, indent=3)
    #['Robbertus', 'NAM', '-', 'B-PERS', 'O', 'I-NARRATIO', 'I-TESTO']
    

    lemmas = [""]+sorted([str(k) for k, v in Counter([x[2] for k,v in principal.items() for x in v]).items() if v>30])
    partes= [""]+sorted([k for k, v in Counter([x[5].split("-")[1] for k,v in principal.items() for x in v]).items()])
    words = [""]+sorted([str(k) for k, v in Counter([x[0] for k,v in principal.items() for x in v]).items() if v>15])

    return render_template("index.html", principal=principal, lemmas=lemmas, partes=partes, words=words)


@app.route('/process',methods=['POST'])
def process():
    if request.method == 'POST':
        partes_retenidas=[]#parte del discurso en cuestion
        partes_total=[]#todas las actas divididas en partes independientemente del termino: lista de listas a 3 elementos (nº del acta, parte, texte)
        partes_anotadas={}#parte en cuestion con entidades nombradas
        partes_error=[]
        choice=[]
        choice_1=[]
        choice_2=[]
        choice_3=[]
        choice_4=[]
        search_term=[]

        if request.form['action'] == 'submit_b':
#-------------------elecciones del usuario-----------------------
            choice = request.form.getlist('Lemmas')[0]
            choice_2 = request.form.getlist('Partes')[0]
            choice_3 = request.form.getlist('Words')[0]
            try:
                choice_4 = request.form.getlist('question')[0]
            except:
                choice_4="names"
    #-----------------------------------------------------------------

            if choice_3!="":
                n,search_term=0, choice_3
            else:
                n,search_term=2, choice

            if choice_4=="names":
                m=3
            elif choice_4=="places":
                m=4
            else:
                m=3


            for k,v in principal.items():
                v=[list(map(str,x)) for x in v]#tranformamos a string cada uno de los componentes de la lista de listas
                partes_post=[x for x in v if x[5].split("-")[1]==choice_2]#aislamos el texto de la partes del discurso
                if any(x[n]==search_term for x in partes_post):#seleccionamos todas las partes del discurso con el lemma buscado
                    index_term=[x[n] for x in partes_post].index(search_term)#index del lema para subrayarlo
                    if index_term!=0:
                        partes_retenidas.append([k, " ".join([str(x[0]) for x in partes_post[:index_term]]), partes_post[index_term][0], " ".join([str(x[0]) for x in partes_post[index_term+1:]])])
                    partes_total.append([[str(k)+"total", key, " ".join([x[0] for x in group])] for key, group in itertools.groupby(v, lambda x: x[5].split("-")[1])])
                    partes_anotadas[k]=[[x[0],x[m]] for x in partes_post]
                    partes_anotadas[k][index_term][1]="spot"#termino subrayado para la pestaña entidades
                    #ojo, con esta funcion se reagrupa una lista de lista por un elemento en comun:
                    #for key, group in groupby(lista_de_listas, lambda x: x[0]):
            if len(partes_retenidas)==0:
                partes_retenidas=["result not found"]#landing para resultado no encontrado
            partes_error=[(k,v) for k,v in Counter([x[5].split("-")[1] for k,v in principal.items() for x in v if x[n]==search_term]).items()]

            return render_template("index.html",partes_retenidas=partes_retenidas, partes_total=partes_total, 
                len=len(partes_retenidas), choice=choice,choice_2=choice_2, choice_3=choice_3, search_term=search_term, 
                partes_anotadas=partes_anotadas, choice_4=choice_4, partes_error=partes_error)



        elif request.form['action'] == 'submit_a':
            partes_total=[]
            partes_retenidas=["exploracion"]

            for k,v in principal.items():
                v=[list(map(str,x)) for x in v]
                partes_total.append([[str(k)+"total", key, [[x[0],x[3]] for x in group]] for key, group in itertools.groupby(v, lambda x: x[5].split("-")[1])])
                #partes_anotadas[k]=[[x[0],x[3]] for x in v]

            return render_template("index.html",partes_retenidas=partes_retenidas, partes_total=partes_total, 
                len=len(partes_retenidas), choice=choice, choice_2=choice_2, choice_3=choice_3, search_term=search_term, 
                partes_anotadas=partes_anotadas, choice_4=choice_4, partes_error=partes_error)

if __name__ == '__main__':
    app.run(host='localhost', port=9874)
### Automatic recognition of diplomatics parts in latin and french text

This application takes raw text or load a file raw text

This project is using a trained Bi-LSTM-CHAR model.

You can explore your corpus by of diplomatics parts using lemma and named entities as filters.

### Requeriments
* Flask==0.12.2
* Jinja2==2.11.2
* joblib==0.14.1

### Deploying the application

The source can be deployed as a web in local using [Flask](https://flask.palletsprojects.com/en/1.1.x/) enviromment.

```
cd \path\ python app.py
localhost:\9999
```

The application was deployed using the GUI from Heroku and a fully-functional online version can be found [here](https://diplomatics-parts.herokuapp.com/).


